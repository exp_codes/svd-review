import csv

from build_model import use_best_feats_on_differ_win_size, use_LRCN_selct_feat_specify_win, \
    select_feature_by_add_in_higher, compare_features_by_g, use_LRCN_selct_feat_specify_win_differ_step, \
    use_selct_feat_specify_win_stepsize, use_LRCN_selct_feat_specify_win_step


# res_csv = open('csv_results/results.csv', 'a')
# writer = csv.writer(res_csv)
# res_csv.close()

#
#

def the_7_exp():
    exp_str = '1234567'
    if '1' in exp_str:  # 使用lrcn对比歌声和混合信号的差别【歌声分离的作用】
        for dataset_h5_path in ['../fin_data_svd/Jamendo/_1.00_vocal.h5',
                                '../fin_data_svd/Jamendo/_1.00_mix.h5',
                                '../fin_data_svd/RWC/_1.00_vocal.h5',
                                '../fin_data_svd/RWC/_1.00_mix.h5',
                                ]:
            use_LRCN_selct_feat_specify_win(dataset_h5_path)
    if '2' in exp_str:  # 对比不同特征使用LRCN的效果【单一特征对比】
        for dataset_h5_path in ['../fin_data_svd/Jamendo/_1.00_vocal.h5',
                                '../fin_data_svd/RWC/_1.00_vocal.h5',
                                ]:
            compare_features_by_g(dataset_h5_path)
    if '3' in exp_str:  # 特征不断增加融合最优特征组合【多特征融合】
        for dataset_h5_path in ['../fin_data_svd/Jamendo/_1.00_vocal.h5',
                                '../fin_data_svd/RWC/_1.00_vocal.h5', ]:
            select_feature_by_add_in_higher(dataset_h5_path)
    if '4' in exp_str:
        for data_dir_item in ['../fin_data_svd/RWC',
                              '../fin_data_svd/Jamendo'
                              ]:
            use_best_feats_on_differ_win_size(data_dir_item)
    if '5' in exp_str:
        for data_dir_item in ['../fin_data_svd/RWC',
                              '../fin_data_svd/Jamendo'
                              ]:
            for step_size in range(5, 30):
                use_LRCN_selct_feat_specify_win_differ_step(data_dir_item + '/_1.00_vocal.h5', step_size)
    if '6' in exp_str:
        for dataset_h5_path in ['../fin_data_svd/Jamendo/_1.00_vocal.h5',
                                '../fin_data_svd/RWC/_1.00_vocal.h5', ]:
            use_selct_feat_specify_win_stepsize(dataset_h5_path)
    if '7' in exp_str:
        for data_dir_item in ['../fin_data_svd/RWC',
                              '../fin_data_svd/Jamendo',
                              '../fin_data_svd/MIR1Ks',
                              '../fin_data_svd/iKalas',
                              '../fin_data_svd/MedleyDBs',
                              ]:
            use_LRCN_selct_feat_specify_win_step(data_dir_item + '/_1.00_vocal.h5')


if __name__ == '__main__':
    for item in range(8):
        the_7_exp()
