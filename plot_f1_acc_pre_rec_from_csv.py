import matplotlib.pyplot as plt
import pandas as pd
import os


def save_plot_bar_svg(csv_path='figure4.csv', ylim_min=0.75, ylim_max=1):
    df = pd.read_csv(csv_path, index_col=0).T
    plt.figure()
    df.plot.bar(rot=0, )
    plt.ylim(ylim_min, ylim_max)
    plt.legend(loc='left', bbox_to_anchor=(1, 1))
    plt.savefig(csv_path.replace('.csv', '_bar.svg'), format='svg', bbox_inches='tight')
    #plt.show()
    return 0


def save_plot_line_svg(csv_path='figure11.csv', ylim_min=0.75, ylim_max=1):
    df = pd.read_csv(csv_path, index_col=0)
    plt.figure()
    df.plot(rot=0, )
    plt.ylim(ylim_min, ylim_max)
    plt.savefig(csv_path.replace('.csv', '_line.svg'), format='svg', bbox_inches='tight')
    #plt.show()
    return 0


def trans_svg_pdf(svg_dir='svg_plots'):
    for item in os.listdir(svg_dir):
        if '.svg' in item:
            svg_path = os.path.join(svg_dir, item)
            cmd_trans = 'inkscape -D -z --file=%s --export-pdf=%s' % (svg_path, svg_path.replace('.svg', '.pdf'))
            print(cmd_trans)
            if not os.path.isfile(svg_path.replace('.svg', '.pdf')):
                os.system(cmd_trans)
    return 0


if __name__ == '__main__':
    csv_dir='svg_plots'
    for item in os.listdir(csv_dir):
        if 'results_compare_features_by_g' in item:
            csv_path=os.path.join(csv_dir,item)
            df_data=pd.read_csv(csv_path,index_col=0)
            min_value_as_low=df_data.min().min()-0.03
            save_plot_bar_svg(csv_path,min_value_as_low,1)
            #save_plot_line_svg(csv_path,min_value_as_low,1)
    trans_svg_pdf(csv_dir)
