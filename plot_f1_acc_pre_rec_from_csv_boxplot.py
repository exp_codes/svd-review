import matplotlib.pyplot as plt
import pandas as pd
import os
from svglib.svglib import svg2rlg
from reportlab.graphics import renderPDF, renderPM




def save_plot_box_svg(csv_path='figure4.csv'):
    print(csv_path)
    df = pd.read_csv(csv_path,index_col=0).T
    plt.figure()
    df.boxplot(grid=False,rot=30)
    plt.ylabel('fmeasure')
    # plt.xlabel('time duration (s)')# fig 13-14
    # plt.xlabel('block size (frames #)')# fig 15-16

    plt.savefig(csv_path.replace('.csv', '_box.svg'), format='svg', bbox_inches='tight')
    # plt.show()
    return 0


def trans_svg_pdf(svg_dir='svg_plots'):
    for item in os.listdir(svg_dir):
        if '.svg' in item:
            svg_path = os.path.join(svg_dir, item)
            drawing = svg2rlg(svg_path)
            renderPDF.drawToFile(drawing, svg_path.replace('.svg','.pdf'))
            renderPM.drawToFile(drawing, svg_path.replace('.svg','.png'), fmt="PNG")
    return 0

def just_save_f1():
    res_dir='multi_times_res'
    for item in os.listdir(res_dir):
        file_path=os.path.join(res_dir,item)
        if '.csv' in file_path:
            with open(file_path,'r') as file_o:
                all_cont=file_o.readlines()
                new_cont=[]
                for line in all_cont:
                    if ',' in line:
                        all_parts=line.split(',')
                        new_cont.append(all_parts[0]+','+all_parts[-1])
                    else:
                        new_cont.append(line)
            print(new_cont)
            with open(file_path, 'w') as file_o:
                file_o.writelines(new_cont)
    return 0

if __name__ == '__main__':
    csv_dir = 'multi_times_res'

    for item in os.listdir(csv_dir):
        if '.csv' in item:
            csv_path = os.path.join(csv_dir, item)
            save_plot_box_svg(csv_path)
            # save_plot_line_svg(csv_path,min_value_as_low,1)
    trans_svg_pdf(csv_dir)
