import os
from shutil import copy

import h5py
import joblib
import numpy
import numpy as np
import pandas as pd
import seaborn as sns
import soundfile
from matplotlib import pyplot as plt
from pydub import AudioSegment

from config import dataset_medlydb_dir, dataset_mir1k_dir, dataset_ikala_dir
from extract_feat import AudioFeat
from svs.unet_svs import split_it


def audio_format_trans():
    return 0


def dataset_medlydb():
    tags_dir = dataset_medlydb_dir + '/Annotations/Melody_Annotations/MELODY2'
    wavs_dir = dataset_medlydb_dir + '/Audio'
    with open('dataset_need_files/medleydb_vocal_songs.txt', 'r') as need_songs_f:
        all_need_songs = need_songs_f.readlines()
    all_song_names = [item.split('_MIX.wav')[0] for item in all_need_songs]
    for song_name in all_song_names:
        wav_path = '%s/%s/%s_MIX.wav' % (wavs_dir, song_name, song_name)
        tag_path = '%s/%s_MELODY2.csv' % (tags_dir, song_name)
        print(wav_path, tag_path)
        print(os.path.isfile(wav_path), os.path.isfile(tag_path))
        des_wav_path = '../MedleyDB/WavFiles/%s' % (os.path.basename(wav_path))
        des_tag_path = '../MedleyDB/tags/%s' % (os.path.basename(tag_path))

        if not os.path.isdir('../MedleyDB/WavFiles/'):
            os.makedirs('../MedleyDB/WavFiles/')
        if not os.path.isdir('../MedleyDB/tags/'):
            os.makedirs('../MedleyDB/tags/')
        copy(wav_path, des_wav_path)
        copy(tag_path, des_tag_path)

    for tag_item in os.listdir('../MedleyDB/tags'):
        tag_path = os.path.join('../MedleyDB/tags', tag_item)
        with open(tag_path, 'r')as tag_f:
            tag_cont = tag_f.readlines()
        start_end_lable = []
        start = None
        lable = None
        for line_item in tag_cont:
            now_time, pitch_value = map(float, line_item.strip('\n').split(','))
            # 使用pitchFile生成歌声标签
            if pitch_value > 0:
                now_lable = 'sing\n'
            else:
                now_lable = 'nosing\n'
            if start == None:
                start = now_time
                lable = now_lable
                end = now_time
            else:
                if lable != now_lable:
                    end = now_time
                    start_end_lable.append('%.3f %.3f %s' % (start, end, lable))
                    start = now_time
                    lable = now_lable
                else:
                    lable = now_lable
                    end = now_time
        start_end_lable.append('%.3f %.3f %s' % (start, end, lable))

        transfer_tag_path = tag_path.replace('/tags', '/transfer_tags').replace('_MELODY2.csv', '.lab')
        transfer_tag_dir = os.path.dirname(transfer_tag_path)
        if not os.path.isdir(transfer_tag_dir):
            os.makedirs(transfer_tag_dir)
        with open(transfer_tag_path, 'w') as ttF:
            ttF.writelines(start_end_lable)

    return 0


def dataset_mir1k():
    pitch_lable_dir = dataset_mir1k_dir + "/PitchLabel"
    for tag_item in os.listdir(pitch_lable_dir):
        tag_path = os.path.join(pitch_lable_dir, tag_item)
        with open(tag_path, 'r')as tag_f:
            tag_cont = tag_f.readlines()
        start_end_lable = []
        start = None
        lable = None
        for line_item in tag_cont:
            now_time, pitch_value = map(float, line_item.strip('\t\n').split('\t'))
            if pitch_value > 0:
                now_lable = 'sing\n'
            else:
                now_lable = 'nosing\n'
            if start == None:
                start = 0
                end = now_time
                lable = now_lable
            else:
                if lable != now_lable:
                    end = now_time
                    start_end_lable.append('%.3f %.3f %s' % (start, end, lable))
                    start = now_time
                    lable = now_lable
                else:
                    end = now_time
                    lable = now_lable
        start_end_lable.append('%.3f %.3f %s' % (start, end, lable))
        transfer_tag_path = tag_path.replace('/PitchLabel', '/tags').replace('.pv', '.lab')
        transfer_tag_dir = os.path.dirname(transfer_tag_path)
        if not os.path.isdir(transfer_tag_dir):
            os.makedirs(transfer_tag_dir)
        with open(transfer_tag_path, 'w') as ttF:
            ttF.writelines(start_end_lable)
    return 0


def dataset_ikala():
    pitch_lable_dir = dataset_ikala_dir + "/PitchLabel"
    for tag_item in os.listdir(pitch_lable_dir):
        tag_path = os.path.join(pitch_lable_dir, tag_item)
        with open(tag_path, 'r')as tag_f:
            tag_cont = tag_f.readlines()
        start_end_lable = []
        start = None
        lable = None
        for index_item, line_item in enumerate(tag_cont):
            now_time = index_item * 30 / len(tag_cont)
            pitch_value = float(line_item.strip('\n'))
            if pitch_value > 0:
                now_lable = 'sing\n'
            else:
                now_lable = 'nosing\n'
            if start == None:
                start = now_time
                end = now_time
                lable = now_lable
            else:
                if lable != now_lable:
                    end = now_time
                    start_end_lable.append('%.3f %.3f %s' % (start, end, lable))
                    start = now_time
                    lable = now_lable
                else:
                    end = now_time
                    lable = now_lable
        start_end_lable.append('%.3f %.3f %s' % (start, end, lable))
        transfer_tag_path = tag_path.replace('/PitchLabel', '/tags').replace('.pv', '.lab')
        transfer_tag_dir = os.path.dirname(transfer_tag_path)
        if not os.path.isdir(transfer_tag_dir):
            os.makedirs(transfer_tag_dir)
        with open(transfer_tag_path, 'w') as ttF:
            ttF.writelines(start_end_lable)
    return 0


def dataset_jamendo():
    return 0


def dataset_rwc():
    return 0


class AudioDatasetPre:

    def load_Dataset_from_h5file(self, h5file_path):
        h5_file = h5py.File(h5file_path, 'r')
        trainX = h5_file['trainX'][:]
        trainY = h5_file['trainY'][:]
        testX = h5_file['testX'][:]
        testY = h5_file['testY'][:]
        validX = h5_file['validX'][:]
        validY = h5_file['validY'][:]
        h5_file.close()
        return trainX, trainY, testX, testY, validX, validY

    def write_Dataset_2_h5file(self, data_set_dir, data_ext_str, vocal_only):
        if vocal_only:
            h5name = os.path.join(data_set_dir, data_ext_str.replace('.joblib', '_vocal.h5'))
            data_ext_str = '.unet.Vocal' + data_ext_str
        else:
            h5name = os.path.join(data_set_dir, data_ext_str.replace('.joblib', '_mix.h5'))
        print("now is writing %s ..." % h5name)
        if os.path.isfile(h5name):
            return
        trainX = []
        trainY = []
        testX = []
        testY = []
        validX = []
        validY = []
        for root, dirs, names in os.walk(data_set_dir):
            for name_item in names:
                if data_ext_str in name_item:
                    if not vocal_only:
                        if '.unet.Vocal' in name_item:
                            continue
                    file_path = os.path.join(root, name_item)
                    data_lable_dict = joblib.load(file_path)
                    data = data_lable_dict['data']
                    lable = data_lable_dict['lable']
                    if '/train/' in file_path:
                        trainX.extend(data)
                        trainY.extend(lable)
                    elif '/test/' in file_path:
                        testX.extend(data)
                        testY.extend(lable)
                    elif '/valid/' in file_path:
                        validX.extend(data)
                        validY.extend(lable)
                    else:
                        raise ('error train_test_valid')

        trainX = np.vstack(trainX)
        testX = np.vstack(testX)
        validX = np.vstack(validX)
        trainY = np.array(trainY)
        testY = np.array(testY)
        validY = np.array(validY)
        file = h5py.File(h5name, 'w')
        file.create_dataset('trainX', data=trainX)
        file.create_dataset('trainY', data=trainY)
        file.create_dataset('testX', data=testX)
        file.create_dataset('testY', data=testY)
        file.create_dataset('validX', data=validX)
        file.create_dataset('validY', data=validY)
        file.close()
        return 0

    def batch_data_gen(self, data_set_dir, data_ext_str, batch_size=50, feat_selc=None, time_steps=30):
        if time_steps != 1:
            x = np.zeros((batch_size, time_steps, 288), dtype=np.uint8)
            y = np.zeros((batch_size, time_steps, 1), dtype=np.uint8)
            time_steps_buffer = []
            time_steps_y = []
            while True:
                batch_size_item = 0
                for root, dirs, names in os.walk(data_set_dir):
                    for name_item in names:
                        if data_ext_str in name_item:
                            file_path = os.path.join(root, name_item)
                            data_lable_dict = joblib.load(file_path)
                            data = data_lable_dict['data']
                            lable = data_lable_dict['lable']
                            for item in range(0, len(data)):
                                Y_item = numpy.array(lable[item])
                                if batch_size_item < batch_size:
                                    if len(time_steps_buffer) < time_steps:
                                        time_steps_buffer.append(data[item])
                                        time_steps_y.append(Y_item)
                                    else:
                                        x[batch_size_item] = numpy.vstack(time_steps_buffer)
                                        y[batch_size_item] = numpy.vstack(time_steps_y)
                                        time_steps_buffer = [data[item]]
                                        time_steps_y = [Y_item]
                                        batch_size_item += 1
                                else:
                                    batch_size_item = 0
                                    xx = x[:, :, feat_selc]
                                    # xx = xx / 1.0 / xx.max(axis=0)
                                    # xx = numpy.nan_to_num(xx)
                                    yield xx, y
        else:
            x = np.zeros((batch_size, 288), dtype=np.uint8)
            y = np.zeros((batch_size), dtype=np.uint8)
            while True:
                batch_size_item = 0
                for root, dirs, names in os.walk(data_set_dir):
                    for name_item in names:
                        if data_ext_str in name_item:
                            file_path = os.path.join(root, name_item)
                            data_lable_dict = joblib.load(file_path)
                            data = data_lable_dict['data']
                            lable = data_lable_dict['lable']

                            for item in range(0, len(data)):
                                Y_item = numpy.array(lable[item])
                                if batch_size_item < batch_size:
                                    x[batch_size_item] = data[item]
                                    y[batch_size_item] = Y_item
                                    batch_size_item += 1
                                else:
                                    batch_size_item = 0
                                    xx = x[:, feat_selc]
                                    # xx = xx / 1.0 / xx.max(axis=0)
                                    # xx = numpy.nan_to_num(xx)
                                    # xx = normalize(xx, axis=0, norm='max')
                                    yield xx, y

    def plot_dataset(self, data_x, lable_y, dim_y):
        sns.set(style="white", color_codes=True)
        df = pd.DataFrame(data_x, columns=['dim_' + str(dim_item) for dim_item in range(data_x.shape[1])])
        df["res_label"] = pd.Series(lable_y)  # .apply(lambda x: "sing" if x == 1 else "nosing")
        sns.catplot(x='res_label', y='dim_' + str(dim_y), data=df, hue="res_label", kind='box')

        plt.savefig('svg_plots/dataset_%s_jamendo_plot.svg' % (str(dim_y)), format='svg')

        return 0

    def view_label_resolution(self, data_dir):
        sing_nosing_resolution = {'sing': [], 'nosing': []}
        lables_dir = '%s/tags' % data_dir
        sing_num = 0
        nosing_num = 0
        sing_total = 0
        nosing_total = 0
        for item in os.listdir(lables_dir):
            lab_path = os.path.join(lables_dir, item)
            with open(lab_path, 'r') as laF:
                lab_cont = laF.readlines()
            for line_item in lab_cont:
                start_end = line_item.split(' ')
                if len(start_end) == 3:
                    start = float(start_end[0])
                    end = float(start_end[1])
                    tag = start_end[2].strip('\n')
                    sing_nosing_resolution[tag].append(end - start)
                    if tag == 'sing':
                        sing_total += 1
                        if (end - start) < 0.3:
                            sing_num += 1
                    elif tag == 'nosing':
                        nosing_total += 1
                        if (end - start) < 0.3:
                            nosing_num += 1

        plt.figure()
        ax0 = plt.subplot(211)
        plt.gca().set_title('sing |<0.3s %d of %d' % (sing_num, sing_total))
        plt.axis([0, 0.5, 0, 100])
        plt.hist(sing_nosing_resolution['sing'], bins=2000)
        plt.subplot(212, sharex=ax0)
        plt.gca().set_title('nosing|<0.3s %d  of %d' % (nosing_num, nosing_total))
        plt.axis([0, 0.5, 0, 100])
        plt.hist(sing_nosing_resolution['nosing'], bins=2000)
        plt.subplots_adjust(hspace=1.0)
        plt.savefig('svg_plots/%s_sing_nosing_time_dur.svg' % data_dir.split('/')[-1], format='svg')
        return sing_nosing_resolution

    def split_vocal(self, data_dir):
        for root, dirs, names in os.walk(data_dir):
            for name in names:
                if '.wav' in name:
                    wav_path = os.path.join(root, name)
                    split_it(wav_path)
        return 0


class Pre_procession_wav:
    def __init__(self, dir_path):
        self.dir_path = dir_path
        self.DataX = []
        self.LabelY = []

    def format_wav(self):
        for root, dirs, names in os.walk(self.dir_path):
            for name in names:
                audio_path = os.path.join(root, name)

                if os.path.basename(audio_path).startswith('.'):
                    os.remove(audio_path)
                    continue
                format_audio = audio_path.split('.')[-1]
                if format_audio == 'lab':  # 跳过标签lable文件
                    continue
                song = AudioSegment.from_file(audio_path, format_audio)
                song = song.set_channels(1)
                song = song.set_frame_rate(16000)
                song = song.set_sample_width(2)
                wav_path = audio_path.replace('.' + format_audio, '.wav')
                os.remove(audio_path)
                song.export(wav_path, 'wav')
                print(wav_path)

    def svs_get_vocal(self):
        # https://gitlab.com/exp_codes/comparisonSVS
        for root, dirs, names in os.walk(self.dir_path):
            for item in names:
                pat_th = os.path.join(root, item)
                if '.wav' in item and '.unet.Vocal.wav' not in item:
                    vocal_path = pat_th.replace('.wav', '.unet.Vocal.wav')
                    print(vocal_path)
                    if not os.path.isfile(vocal_path):
                        split_it(pat_th)
                    # os.remove(pat_th)

    def extract_feat_from_wav(self, win_size, hop_size, vocal_only):
        for root, dirs, names in os.walk(self.dir_path):
            for name in names:

                DataX = []
                LabelY = []
                wav_path = os.path.join(root, name)
                if vocal_only:
                    if '.unet.Vocal.' not in wav_path:
                        continue
                wav_ext_str = '.' + wav_path.split('.')[-1]
                if wav_ext_str in '.lab.joblib':
                    continue  # 跳过lable
                feat_path = wav_path.replace('.wav', '_%.2f.joblib' % win_size)
                if os.path.isfile(feat_path):
                    continue  # 跳过已提取
                print(feat_path)
                signal, samplerate = soundfile.read(wav_path)
                win_size_num = int(win_size * samplerate)  # 0.01s to nums
                hop_size_num = int(hop_size * samplerate)
                if '/train/' in wav_path:
                    lab_path = wav_path.replace('/train/', '/tags/').replace(wav_ext_str, '.lab')
                elif '/test/' in wav_path:
                    lab_path = wav_path.replace('/test/', '/tags/').replace(wav_ext_str, '.lab')
                elif '/valid/' in wav_path:
                    lab_path = wav_path.replace('/valid/', '/tags/').replace(wav_ext_str, '.lab')
                if '.unet.Vocal.' in lab_path:
                    lab_path = lab_path.replace('.unet.Vocal.', '.')
                if '_MIX.lab' in lab_path:
                    lab_path=lab_path.replace('_MIX.lab', '.lab')
                lab_contend = open(lab_path)
                lab_contend_lines = lab_contend.readlines()
                lab_contend.close()
                for line in lab_contend_lines:
                    line_item_list = line.split(' ')
                    start_time = float(line_item_list[0])
                    end_time = float(line_item_list[1])
                    lab_sing_nosing = line_item_list[2][:-1]
                    part_signal = signal[int(start_time * samplerate):int(end_time * samplerate)]
                    try:
                        audio_feater = AudioFeat(win_size_n=win_size_num, hop_size_n=hop_size_num)
                        part_signal_feat = audio_feater.get_audio_feature(part_signal, samplerate)
                        DataX.extend(part_signal_feat)
                        f = lambda x: 1 if x == 'sing' else 0
                        LabelY.extend([f(lab_sing_nosing)] * len(part_signal_feat))
                    except:
                        print('song:%s start:%s end:%s feat extract jump the part signals ...' % (
                            wav_path, start_time, end_time))
                joblib.dump({'data': DataX, 'lable': LabelY}, feat_path, compress=3)


if __name__ == '__main__':
    do_str = ''
    if '1' in do_str:  # 提取所有特征到中间文件joblib_file

        for dataset in ['MedleyDBs','MIR1Ks', ]: #['Jamendo', 'RWC']:  # , 'MIR1K', 'iKala', 'MedleyDB']:
            dataset_dir_path = '../fin_data_svd/%s' % dataset
            preprocess_wav = Pre_procession_wav(dataset_dir_path)
            # preprocess_wav.format_wav()
            # preprocess_wav.svs_get_vocal()

            # for win_size in [0.05, 0.10, 0.20, 0.30, 0.50, 0.80, 1, 2, 3]:
            #     hop_size = 0.04
            #     preprocess_wav.extract_feat_from_wav(win_size, hop_size,True)

            preprocess_wav.extract_feat_from_wav(1, 0.04, True)


    if '2' in do_str:  # 保存为数据集中间文件h5file
        for data_dir in ['MIR1Ks', 'iKalas', 'MedleyDBs']:# ['Jamendo', 'RWC']:  # , 'Jamendo','MIR1K', 'iKala', 'MedleyDB']:
            for ext_str in [1]:  # , 0.10, 0.20, 0.30, 0.50, 0.80, 1, 2]:  # , 3]:# 3s is too long to all empty data
                data_set_dir = '../fin_data_svd/%s' % data_dir
                data_ext_str = '_%.2f.joblib' % ext_str
                for vocal_only in [True]:  # True, False]:
                    dataset_saver = AudioDatasetPre()
                    dataset_saver.write_Dataset_2_h5file(data_set_dir, data_ext_str, vocal_only)
