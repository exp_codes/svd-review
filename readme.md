# Singing voice detection 

## Divided by module

* config.py parameter settings
* preprocess.py Dataset preprocessing audio file and division, singing voice separation weaken accompaniment
* extract_feat.py Multiple feature extraction
* build_model.py Different machine learning model construction
* evaluation.py Evaluation index calculation results
* best_param_svd.py Main program entry
* plot_f1_acc_pre_rec_from_csv.py Plot the output result csv into an svg diagram
* api.py The API, submits the audio generation time segmentation corresponding to the singing voice is not the singing voice

## Experiment content:

1. Experiment dataset finishing, 5 datasets【Jamendo，RWC，MIR1K，iKala，MedleyDB】
2. Comparison of singing voice separation
3. Frame size for feature,【40ms，100ms，200ms，300ms，500ms，800ms，1100ms，1500ms，2000ms，3000ms】
4. Feature combination
5. Comparison of traditional classifiers,【Random forest,GMM，SVM，xgboost】
6. Deep model comparison,【dnn，lstm，gru，cnn】
7. Smooth post-processing, segmentation requires post-processing, detection does not require post-processing

## Data files @20200129

* The dataset has been processed in ../fin_data_svd，Among them, rwc and Jamendo have generated features，MIR1K，iKala，MedleyDBFeatures to be extracted.
